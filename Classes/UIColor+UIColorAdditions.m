//
//  UIColor+UIColorAdditions.m
//  Radwerkstatt
//
//  Created by Matthias Wenz on 03.02.12.
//  Copyright (c) 2012 Bowstreet GmbH. All rights reserved.
//

#import "UIColor+UIColorAdditions.h"

@implementation UIColor (UIColorAdditions)

+ (UIColor *)colorWithRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue {
    return [UIColor colorWithRed:red/255. green:green/255. blue:blue/255. alpha:1.0];
}

+ (UIColor *)colorWithHexValue:(uint)hexValue alpha:(float)alpha {
    return [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.f
                           green:((float)((hexValue & 0xFF00) >> 8))/255.f
                            blue:((float)(hexValue & 0xFF))/255.f
                           alpha:alpha];
}

+ (UIColor *)colorWithHexString:(NSString*)hexString alpha:(float)alpha {
    UIColor *resultColor;
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#"
                                                     withString:@"0x"];
    uint hexValue;
    if ([[NSScanner scannerWithString:hexString] scanHexInt:&hexValue]) {
        resultColor = [self colorWithHexValue:hexValue alpha:alpha];
    } else {
        [NSException raise:@"InvalidColorValue" format:@"Invalid hex color value: %@", hexString];
    }
    return resultColor;
}

+ (UIColor *)colorWithHexString:(NSString *)hexString {
    return [self colorWithHexString:hexString alpha:1.0];
}

@end
