//
//  BOWCache.m
//  Bootstrap
//
//  Created by Matthias Wenz on 20.10.13.
//
//

#import "BOWCache.h"

NSString *const BOWCacheStatisticReadsName = @"bowcache.stats.reads";
NSString *const BOWCacheStatisticHitsName = @"bowcache.stats.hits";
NSString *const BOWCacheStatisticMissesName = @"bowcache.stats.misses";
NSString *const BOWCacheStatisticHitRatioName = @"bowcache.stats.hit-ratio";
NSString *const BOWCacheStatisticMissRatioName = @"bowcache.stats.miss-ratio";

@interface BOWCache () {
    NSUInteger __reads, __hits, __misses;
}

@end

@implementation BOWCache

- (id)objectForKey:(id)key {
    id value = [super objectForKey:key];
    __reads++;
    
    if (value == nil) {
        __misses++;
    } else {
        __hits++;
    }
    
    return value;
}

- (NSDictionary *)cacheStatistics {
    return @{
             BOWCacheStatisticReadsName: [NSNumber numberWithUnsignedInteger:__reads],
             BOWCacheStatisticHitsName: [NSNumber numberWithUnsignedInteger:__hits],
             BOWCacheStatisticMissesName: [NSNumber numberWithUnsignedInteger:__misses],
             BOWCacheStatisticHitRatioName: [NSNumber numberWithDouble:((double)__hits/(double)__reads)],
             BOWCacheStatisticMissRatioName: [NSNumber numberWithDouble:((double)__misses/(double)__reads)]
             };
}

@end
