//
//  UIColor+UIColorAdditions.h
//  Radwerkstatt
//
//  Created by Matthias Wenz on 03.02.12.
//  Copyright (c) 2012 Bowstreet GmbH. All rights reserved.
//

#define BOW_RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

@interface UIColor (UIColorAdditions)

+ (UIColor *)colorWithRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue;
+ (UIColor *)colorWithHexValue:(uint)hexValue alpha:(float)alpha;
+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(float)alpha;
+ (UIColor *)colorWithHexString:(NSString *)hexString;


@end
