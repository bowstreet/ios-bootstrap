//
//  BOWStringTools.m
//  Bootstrap
//
//  Created by Matthias Wenz on 16.10.13.
//
//

#import "BOWStringTools.h"

@implementation BOWStringTools

+ (NSString *)stringForProgress:(double)progress {
    return [self stringForProgress:progress withLength:10];
}

+ (NSString *)stringForProgress:(double)progress withLength:(NSUInteger)length {
    NSMutableString *resultString = [NSMutableString new];
    
    [resultString appendFormat:@"(%@)", [self buildProgressString:progress withLength:length]];
    
    return [NSString stringWithString:resultString];
}

+ (NSString *)buildProgressString:(double)progress withLength:(NSUInteger)length {
    NSUInteger value = roundl(progress * length);
    
    NSMutableString *resultString = [NSMutableString new];
    for (NSUInteger i = 0; i < length; i++) {
        if (i < value-1) {
            [resultString appendString:@"="];
        } else if (i == value-1) {
            [resultString appendString:@"0"];
        } else {
            [resultString appendString:@"-"];
        }
    }
    return resultString;
}

+ (NSString *)stringForProgress:(double)progress total:(double)total {
    return [self stringForProgress:progress total:total withLength:10];
}

+ (NSString *)stringForProgress:(double)progress total:(double)total withLength:(NSUInteger)length {
    return [self stringForProgress:(progress/total) withLength:length];
}

@end
