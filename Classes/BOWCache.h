//
//  BOWCache.h
//  Bootstrap
//
//  Created by Matthias Wenz on 20.10.13.
//
//

FOUNDATION_EXPORT NSString *const BOWCacheStatisticReadsName;
FOUNDATION_EXPORT NSString *const BOWCacheStatisticHitsName;
FOUNDATION_EXPORT NSString *const BOWCacheStatisticMissesName;
FOUNDATION_EXPORT NSString *const BOWCacheStatisticHitRatioName;
FOUNDATION_EXPORT NSString *const BOWCacheStatisticMissRatioName;

@interface BOWCache : NSCache

@property (nonatomic, readonly) NSDictionary *cacheStatistics;

@end
