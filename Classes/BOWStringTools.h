//
//  BOWStringTools.h
//  Bootstrap
//
//  Created by Matthias Wenz on 16.10.13.
//
//

@interface BOWStringTools : NSObject

+ (NSString *)stringForProgress:(double)progress;
+ (NSString *)stringForProgress:(double)progress withLength:(NSUInteger)length;

+ (NSString *)stringForProgress:(double)progress total:(double)total;
+ (NSString *)stringForProgress:(double)progress total:(double)total withLength:(NSUInteger)length;

@end
