//
//  BOWCache_Tests.m
//  Bootstrap
//
//  Created by Matthias Wenz on 20.10.13.
//
//

#import <XCTest/XCTest.h>

#import "BOWCache.h"

@interface BOWCache_Tests : XCTestCase

@property (nonatomic, strong) BOWCache *cache;

@end

@implementation BOWCache_Tests

- (void)setUp {
    [super setUp];
    
    _cache = [BOWCache new];
}

- (void)testCacheName {
    static NSString *name = @"de.bowstreet.test-cache";
    XCTAssert([_cache.name isEqualToString:@""], @"Name should be empty.");
    _cache.name = name;
    XCTAssertTrue([_cache.name isEqualToString:name], @"Cache name %@ does not equal expected name %@", _cache.name, name);
}

- (void)testCacheFunctionality {
    NSDictionary *toCache = @{
                              @"a": @1,
                              @"b": @2,
                              @"c": @3
                              };
    
    
    NSDictionary *stats = _cache.cacheStatistics;
    
    XCTAssert([[stats objectForKey:BOWCacheStatisticReadsName] isEqualToNumber:@0], @"Should have been no reads.");
    
    for (NSString *key in [toCache allKeys]) {
        XCTAssertNil([_cache objectForKey:key], @"Value should not be cached for key: %@", key);
    }
    
    stats = _cache.cacheStatistics;
    XCTAssert([[stats objectForKey:BOWCacheStatisticReadsName] isEqualToNumber:@3], @"Should have been three reads.");
    XCTAssert([[stats objectForKey:BOWCacheStatisticHitsName] isEqualToNumber:@0], @"Should have been no hits.");
    XCTAssert([[stats objectForKey:BOWCacheStatisticMissesName] isEqualToNumber:@3], @"Should have been three misses.");
    XCTAssert([[stats objectForKey:BOWCacheStatisticHitRatioName] isEqualToNumber:@0.0], @"Hit-ratio should be 0.0.");
    XCTAssert([[stats objectForKey:BOWCacheStatisticMissRatioName] isEqualToNumber:@1.0], @"Miss-ratio should be 1.0.");
    
    
    for (NSString *key in [toCache allKeys]) {
        [_cache setObject:[toCache objectForKey:key] forKey:key];
    }
    
    for (NSString *key in [toCache allKeys]) {
        XCTAssertNotNil([_cache objectForKey:key], @"Value should be cached for key: %@", key);
    }
    
    stats = _cache.cacheStatistics;
    XCTAssert([[stats objectForKey:BOWCacheStatisticReadsName] isEqualToNumber:@6], @"Should have been six reads.");
    XCTAssert([[stats objectForKey:BOWCacheStatisticHitsName] isEqualToNumber:@3], @"Should have been three hits.");
    XCTAssert([[stats objectForKey:BOWCacheStatisticMissesName] isEqualToNumber:@3], @"Should have been three misses.");
    XCTAssert([[stats objectForKey:BOWCacheStatisticHitRatioName] isEqualToNumber:@0.5], @"Hit-ratio should be 0.5.");
    XCTAssert([[stats objectForKey:BOWCacheStatisticMissRatioName] isEqualToNumber:@0.5], @"Miss-ratio should be 0.5.");
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

@end
