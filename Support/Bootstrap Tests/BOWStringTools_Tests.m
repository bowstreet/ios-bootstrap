//
//  BOWStringTools_Tests.m
//  Bootstrap
//
//  Created by Matthias Wenz on 16.10.13.
//
//

#import <XCTest/XCTest.h>

#import "BOWStringTools.h"

@interface BOWStringTools_Tests : XCTestCase

@end

@implementation BOWStringTools_Tests

- (void)testProgressGeneratorSimple {
    NSDictionary *rig = @{
      @0.1: @"(0---------)",
      @0.5: @"(====0-----)",
      @1.0: @"(=========0)",
      };
    
    for (NSNumber *key in [rig allKeys]) {
        NSString *test = [BOWStringTools stringForProgress:[key doubleValue]];
        NSString *expect = rig[key];
        XCTAssertTrue([test isEqualToString:expect], @"Expected %@ for %@ - not %@", expect, key, test);
    }
}

- (void)testProgressGeneratorExtended {
    NSString *expect = @"(=========0----------)";
    NSString *test = [BOWStringTools stringForProgress:4 total:8 withLength:20];
    XCTAssertTrue([test isEqualToString:expect], @"Expected %@ - not %@", expect, test);
}

@end
