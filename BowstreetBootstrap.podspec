Pod::Spec.new do |s|
  s.name         = "BowstreetBootstrap"
  s.version      = "0.0.3"
  s.summary      = "A sensible starting point for apps from Bowstreet."
  s.homepage     = "http://bowstreet.de/"

  s.author       = { "Matthias Wenz" => "mat@bowstreet.de" }
  s.source       = { :git => "https://github.com/bowstreet/ios-bootstrap.git", :tag => s.version.to_s }
  s.platform     = :ios, '5.0'
  s.license      = 'MIT'

  s.source_files = 'Classes'
  s.requires_arc = true
  s.preserve_paths = 'Resources', 'Support'
  s.frameworks   = 'UIKit'

  s.xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => %{$(inherited) CONFIGURATION_$(CONFIGURATION)} }

end
